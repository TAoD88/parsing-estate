import requests
from datetime import datetime
import os
from bs4 import BeautifulSoup as BS
import json


### Создаем папку для файла(ов)
def make_dir(url):
    name_for_dir = 'data/' + url.replace('https://', '').split('.')[0]
    # Создаем место для хранения файлов если его нет
    if not os.path.isdir(name_for_dir):
        os.makedirs(name_for_dir)
        return f"Папка {url.split('/')[-1]} в директории parsing_app/data Cоздана!"
    return f"Папка {url.split('/')[-1]} в директории parsing_app/data уже существует"


###  Получаем количество страниц для парсинга
def number_of_pages(url) -> int:
    headers = {
        "accept": "*/*",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36"

    }
    req = requests.get(url.format(1), headers=headers)
    src = req.text  # перегоняем в читаемый формат
    soup = BS(src, "lxml")
    pages_count = soup.find("ul", class_="pagination").find_all("a")[-2].text  # получаем число страниц
    return int(pages_count)


### Создаем лист ссылок для парсинга
def save_url(url) -> list:
    # pages = number_of_pages(url.format(1))
    pages = 1
    list_url_pages = []  # создаем список ссылок
    for each in range(1, pages + 1):
        headers = {
            "accept": "*/*",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36"
        }
        req = requests.get(url.format(each), headers=headers)  # получаем код исходной страницы
        src = req.text  # перегоняем в читаемый формат
        soup = BS(src, "lxml")
        body = soup.find("div", class_="objects-list switchable listview").find('ul').find_all("li")
        # забираем и всех ссылок нужную нам
        for each in body:
            tag_a = each.find_all("a")
            try:
                mane = tag_a[0].get("href")
                list_url_pages.append(mane)
            except:
                continue
    # print(list_url_pages)
    return list_url_pages


### заносим дикт в список
def get_info(list_pages):
    dir_file = 'data/' + list_pages[0].replace('https://', '').split('.')[0]  # путь
    name_for_file = list_pages[0].replace('https://', '').split('.')[0]  # имя файла
    cur_time = datetime.now().strftime("%d_%m_%Y")
    count = 0  # подсчет
    list_info = []  # создаем конечный список
    for each_url in list_pages:
        count += 1
        page_dict = {}  # Создаем словать
        headers = {
            "accept": "*/*",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36",
            "Cookie": "kits_currency=USD"

        }
        req = requests.get(each_url, headers=headers)
        src = req.text  # перегоняем в читаемый формат
        soup = BS(src, "lxml")
        page_dict['url_object'] = str(each_url)  # ссылка на страницу
        h1 = soup.find('h1').text
        top_info = h1.split('№')
        page_dict['name_object'] = str(top_info[0].strip())  # название объекта
        page_dict['id_object'] = str(top_info[1].strip())  # ID объекта

        body = soup.find("div", class_="aside")
        params = body.find("div", class_="right_block parameters").find_all("div")
        # пробегаемся по параметрам и парсим нужную нам информацию
        for each in params[1:]:
            tag_span = each.find_all("span")
            try:
                name_odj = ''.join(each.get('class'))
                value_obj = str(tag_span[1].text.replace('\xa0', ''))
                if name_odj == "square":
                    page_dict[name_odj] = value_obj[:-6]
                else:
                    page_dict[name_odj] = value_obj
            except:
                continue
        # Собираем все в конечный список
        list_info.append(page_dict)

    print(f"Обработанно {count} объектов")
    ## Сохраняем в файл JSON
    with open(f"{dir_file}/{name_for_file}_{cur_time}.json", "w", encoding="utf-8") as file:
        json.dump(list_info, file, indent=4, ensure_ascii=False)
