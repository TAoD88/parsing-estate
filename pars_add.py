from emirates_pars import save_url, make_dir, get_info
import time


def main():
    start_time = time.time()
    url_emirates = 'https://emirates.estate/ru/property/page/{}/#objects'
    road = make_dir(url_emirates)
    list_pages = save_url(url_emirates)
    get_info(list_pages)

    print('Папки созданы и файлы сохранены ...')
    full_time = time.time() - start_time
    print(f'На работу скрипта потрачено: {full_time} сек.')


if __name__ == "__main__":
    main()
