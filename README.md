![iconH](for readme/Без названия.jpg "Icon")

# Parsing estate

___

## О проекте

Задача была поставлена спарсит с 3-х сайтов недвижимости все данные и записать их для дальнейшей интеграции.

Так как это коммерческий проект, то здесь представлена только часть кода на примере одного сайта

___

## Ресурсы

Сайты:

1) https://emirates.estate
2) https://dubai.dubizzle.com
3) https://www.propertyfinder.ae

Библиотеки:
- [X] BeautifulSoup
- [X] Lxml
- [X] Json

## Немного кода
___
- [X] Основной блок который запускает все остальные

```python
def main():
    start_time = time.time()
    url_emirates = 'https://emirates.estate/ru/property/page/{}/#objects'
    road = make_dir(url_emirates)
    list_pages = save_url(url_emirates)
    get_info(list_pages)
```

- [X] Пример части блока pars

```python
### Создаем лист ссылок для парсинга
def save_url(url) -> list:
    # pages = number_of_pages(url.format(1))
    pages = 5
    list_url_pages = []  # создаем список ссылок
    for each in range(1, pages + 1):
        headers = {
            "accept": "*/*",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36"
        }
        req = requests.get(url.format(each), headers=headers)  # получаем код исходной страницы
        src = req.text  # перегоняем в читаемый формат
        soup = BS(src, "lxml")
        body = soup.find("div", class_="objects-list switchable listview").find('ul').find_all("li")
        # забираем и всех ссылок нужную нам
        for each in body:
            tag_a = each.find_all("a")
            try:
                mane = tag_a[0].get("href")
                list_url_pages.append(mane)
            except:
                continue
    # print(list_url_pages)
    return list_url_pages
```

## Немного результата

```json
[
    {
        "url_object": "https://emirates.estate/ru/property/o45496/",
        "name_object": "Квартира в Дубай Марина, ОАЭ 2 спальни, 148.6м2",
        "id_object": "45496",
        "city": "Дубай",
        "region": "Дубай Марина",
        "tip": "Квартира",
        "rooms": "2",
        "square": "148.6 м²",
        "price_info": "6250000 AED"
    },
    {
        "url_object": "https://emirates.estate/ru/property/o44975/",
        "name_object": "Квартира в Умм-Сукейм, Дубай, ОАЭ 1 спальня, 76.1м2",
        "id_object": "44975",
        "city": "Дубай",
        "region": "Умм-Сукейм",
        "tip": "Квартира",
        "rooms": "1",
        "square": "76.1 м²",
        "price_info": "1510000 AED"
    }
]
```
